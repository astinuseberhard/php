#!/bin/bash

###########################################################
# Builds target PHP version                               #
#                                                         #
# @author Piotr Makowski                                  #
# @version 1.0.1                                          #
###########################################################

#echo -n "variable does not exists test: "
#[[ -v TEST_TEST ]] && ( echo -e "\e[7;31m ERR \e[0m" ; exit 1 ) || echo -e "\e[7;32m OK \e[0m"
#declare -i TEST_TEST= ; echo -n "variable DOES exists test: "
#[[ -v TEST_TEST ]] || ( echo -e "\e[7;31m ERR \e[0m" ; exit 1 ) && echo -e "\e[7;32m OK \e[0m" ; exit 0

# init
RUNDIR=$(readlink -fn .)
BINDIR=$(dirname $(readlink -fn $0))
JOBS=1

while getopts j:frgAiIXsuvhcldt OPT ; do case "$OPT" in
    j) JOBS="${OPTARG}" ;;
    f) declare FETCH_ORIGIN= ;;
    c) declare INSTALL_SYSTEM_REQUIREMENTS= ;;
    r) declare RECOMPILE_PHP= ;;
    A) declare WITHOUT_APCU= ;;
    X) declare WITHOUT_XDEBUG= ;;
    s) declare WITH_SYCK= ;;
    g) declare WITH_GEARMAN= ;;
    l) declare WITH_SWOOLE= ;;
    d) declare WITH_REDIS= ;;
    t) declare WITH_TIMECOP= ;;
    i) declare ONLY_INI= ;;
    u) declare UPDATE_ALTERNATIVES= ;;
    v) declare ALL_TAGS= ;;
    i) declare WITHOUT_IMAGICK= ;;
    *) declare SHOW_USE= ;;
esac ; done ; shift $(( OPTIND - 1 ))

git --version | grep -q 'git version 1' && echo -e '\e[0;31mGIT version 2+ required\e[0m' && exit 1

[ -d $BINDIR/php-src ] || git clone -n https://git.php.net/repository/php-src.git $BINDIR/php-src
[ -d $BINDIR/xdebug ] || git clone -b XDEBUG_2_5_3 https://github.com/xdebug/xdebug.git $BINDIR/xdebug
[ -d $BINDIR/syck ] || git clone https://github.com/indeyets/syck.git $BINDIR/syck
[ -d $BINDIR/apcu ] || git clone -b v5.1.8 https://github.com/krakjoe/apcu.git $BINDIR/apcu
[ -d $BINDIR/gearman ] || git clone https://github.com/wcgallego/pecl-gearman.git $BINDIR/gearman
[ -d $BINDIR/swoole ] || git clone https://github.com/swoole/swoole-src.git $BINDIR/swoole
[ -d $BINDIR/redis ] || git clone https://github.com/phpredis/phpredis.git $BINDIR/redis
[ -d $BINDIR/timecop ] || git clone https://github.com/hnw/php-timecop.git $BINDIR/timecop
[ -d $BINDIR/imagick ] || git clone https://github.com/Imagick/imagick.git $BINDIR/imagick

if [[ -v INSTALL_SYSTEM_REQUIREMENTS ]]; then
    sudo apt install $( cat requirements )
fi

# echo "Given flags: "
# set | grep -P 'RUNDIR|BINDIR|JOBS|FETCH_ORIGIN|INSTALL_SYSTEM_REQUIREMENTS|RECOMPILE_PHP|WITH(OUT)?_|ONLY_INI|UPDATE_ALTERNATIVES|ALL_TAGS|SHOW_USE'
# read -n 1 -t 5

if [[ -v SHOW_USE || $# -eq 0 ]]; then

    HEADER='\e[4;93m'
    NOT_EXISTS='\e[3;90m'
    FAILED='\e[1;91m'
    ALTERED='\e[93m'
    OK='\e[32m'
    NO_STABLE_VERSION='\e[2;3;37m no stable versions available\e[0m'

    NEW='\e[0;32m'
    SUPPORTED='\e[0;33m'
    OLD='\e[0;31m'
    NO_STABLE='\e[0;34m'


    [ -v FETCH_ORIGIN ] && echo -e '\e[7;33m Fetching sources, please wait... \e[0m' && git -C $BINDIR/php-src fetch -t >>/dev/null 2>&1
    echo 'Usage: ./build [-h?] [-f] [-c] [ [-i | -r] [-AXsgl] [-u] [-j <jobs> ] <php_version_tag> ]'
    echo
    echo -e "${HEADER}Switches:\e[0m"
    echo "-j     run as many parallel Make jobs during compilation"
    echo "-f     fetch PHP source from it's origin repository"
    echo "-c     install system requirements"
    echo "-i     do not compile anything, just generate ini file"
    echo '-A     without APCu'
    echo '-s     with Syck'
    echo '-X     without XDebug'
    echo '-I     without Imagick'
    echo '-g     with Gearman'
    echo '-l     with Swolle'
    echo '-d     with Redis'
    echo '-t     with Timecop'
    echo '-r     force to recompile PHP build if already exists'
    echo '-u     update alternatives for selected build'
    echo '-v     show all PHP versions (default is stable only)'
    echo '-? -h  display this message'
    echo
    echo -e "${HEADER}Available versions:\e[0m"
    for major in $(git -C php-src tag | grep -oP '^php-\d+\.\d+' | sort -V | uniq) ; do
        [ -v ALL_TAGS ] \
            && versions=($(git -C php-src tag | grep -P "^$major\." | sort -V)) \
            || versions=($(git -C php-src tag | grep -P "^$major\.\d+\$" | sort -V))
        isNew=$([[ ${#versions[@]} -gt 0 ]] && git -C php-src log ${versions[0]} --after=='2 years ago' -n1 --oneline)
        isSupported=$([[ ${#versions[@]} -gt 0 ]] && git -C php-src log ${versions[-1]} --after=='3 months ago' -n1 --oneline)
        if [[ $isNew ]] ; then echo -ne "$NEW" ;
        elif [[ $isSupported ]] ; then echo -ne "$SUPPORTED" ;
        elif [[ ${#versions[@]} -gt 0 ]] ; then echo -ne "$OLD" ;
        else echo -ne "$NO_STABLE" ; fi
        echo -ne "PHP $major:\e[0m"
        if [[ ${#versions[@]} -gt 0 ]]; then
            for i in ${versions[@]}; do
                # check that build directory exists
                [ -d $i ] \
                    && ( \
                        [ -x $i/bin/php ] \
                        && ( $i/bin/php -v | grep -i "$( echo $i | sed 's/php-//')" >/dev/null 2>&1 ) \
                        && ( \
                            [ -r $i/lib/php.ini ] && $i/bin/php --ini | grep $i/lib/php.ini >/dev/null 2>&1 \
                            && echo -ne " $OK" \
                            || echo -ne " $ALTERED" \
                        ) \
                        || echo -ne " $FAILED" \
                    ) \
                    || echo -ne " $NOT_EXISTS"
                echo -ne "$i\e[0m" # version (tag) output
            done
        else
            echo -ne "$NO_STABLE_VERSION"
        fi
        echo
    done
    echo
    echo -e "Legend:  ${NOT_EXISTS}build not exists\e[0m   ${FAILED}build failed\e[0m   ${OK}build ok\e[0m   ${ALTERED}build altered\e[0m"
else

    OK='\e[7;32m'
    OMMIT='\e[7;33m'
    ERROR='\e[7;31m'

    ( [ -d $BINDIR/$1 ] && [ ! -v RECOMPILE_PHP ] || [ -v ONLY_INI ] ) \
    && echo -e "$OMMIT PHP core deploy omitted \e[0m" \
    || ( \
        echo -e "$OK Compiling PHP core... \e[0m" ; \

        cd $BINDIR/php-src && git add -A && git reset --hard && git checkout $1 && \
        ( make clean ; ./buildconf --force ) && ( [ ! -x ./genfiles ] || ./genfiles ) && \
        ./configure --prefix=$BINDIR/$1 $( cat $BINDIR/configure.default ) && \
        make -j $JOBS && make install || { echo -e "$ERROR Failed to compile PHP core \e[0m" ; exit -1 ; } \
    )

    [ -v UPDATE_ALTERNATIVES ] \
    && echo -e "$OK Updating system alternatives \e[0m" \
    && ( \
        [ -x "$1/bin/php" ] && sudo update-alternatives --install /usr/bin/php php "$( readlink -f "$1/bin/php" )" 0 && sudo update-alternatives --set php "$( readlink -f "$1/bin/php" )"
        [ -x "$1/sbin/php-fpm" ] && sudo update-alternatives --install /sbin/php-fpm php-fpm "$( readlink -f "$1/sbin/php-fpm" )" 0 && sudo update-alternatives --set php-fpm "$( readlink -f "$1/sbin/php-fpm" )" && sudo systemctl restart nginx.service apache2.service php-fpm.service
    )

    ( [ -v WITHOUT_XDEBUG ] || [ -v ONLY_INI ] ) \
    && echo -e "$OMMIT XDebug deploy omitted \e[0m" \
    || ( \
        echo -e "$OK Compiling XDebug... \e[0m" ; \
        cd $BINDIR/xdebug && ( make clean ; $BINDIR/$1/bin/phpize ) && \
        ./configure --enable-xdebug --with-php-config=$BINDIR/$1/bin/php-config && \
        make -j $JOBS && make install || declare WITHOUT_XDEBUG= \
    )

    ( [ ! -v WITH_SYCK ] || [ -v ONLY_INI ] ) \
    && echo -e "$OMMIT Syck deploy omitted \e[0m" \
    || ( \
        echo -e "$OK Compiling Syck extension... \e[0m" ; \
        cd $BINDIR/syck && \
        ( ./bootstrap ; ./configure ; make ; ln -nsf lib include ) && cd ext/php && \
        ( make clean ; $BINDIR/$1/bin/phpize ) && \
        ./configure --with-syck=../.. --with-php-config=$BINDIR/$1/bin/php-config && \
        make -j $JOBS && make install || unset WITH_SYCK \
    )

    ( [ -v WITHOUT_APCU ] || [ -v ONLY_INI ] ) \
    && echo -e "$OMMIT APCu deploy omitted \e[0m" \
    || ( \
        echo -e "$OK Compiling APCu extension... \e[0m" ; \
        cd $BINDIR/apcu && ( make clean ; $BINDIR/$1/bin/phpize ) && \
        ./configure --with-php-config=$BINDIR/$1/bin/php-config && \
        make -j $JOBS && make install || declare WITHOUT_APCU= \
    )

    ( [ ! -v WITH_GEARMAN ] || [ -v ONLY_INI ] ) \
    && echo -e "$OMMIT GEARMAN deploy omitted \e[0m" \
    || ( \
        echo -e "$OK Compiling GEARMAN extension... \e[0m" ; \
        cd $BINDIR/gearman && ( make clean ; $BINDIR/$1/bin/phpize ) && \
        ./configure --with-php-config=$BINDIR/$1/bin/php-config && \
        make -j $JOBS && make install || unset WITH_GEARMAN \
    )

    ( [ ! -v WITH_SWOOLE ] || [ -v ONLY_INI ] ) \
    && echo -e "$OMMIT Swoole deploy omitted \e[0m" \
    || ( \
        echo -e "$OK Compiling Swoole extension... \e[0m" ; \
        cd $BINDIR/swoole && ( make clean ; $BINDIR/$1/bin/phpize ) && \
        ./configure --with-php-config=$BINDIR/$1/bin/php-config --enable-sockets --enable-openssl --enable-http2 --enable-mysqlnd && \
        make -j $JOBS && make install || unset WITH_SWOOLE \
    )

    ( [ ! -v WITH_REDIS ] || [ -v ONLY_INI ] ) \
    && echo -e "$OMMIT Redis deploy omitted \e[0m" \
    || ( \
        echo -e "$OK Compiling Redis extension... \e[0m" ; \
        cd $BINDIR/redis && ( make clean ; $BINDIR/$1/bin/phpize ) && \
        ./configure --with-php-config=$BINDIR/$1/bin/php-config  && \
        make -j $JOBS && make install || unset WITH_REDIS \
    )

    ( [ ! -v WITH_TIMECOP ] || [ -v ONLY_INI ] ) \
    && echo -e "$OMMIT Timecop deploy omitted \e[0m" \
    || ( \
        echo -e "$OK Compiling Timecop extension... \e[0m" ; \
        cd $BINDIR/timecop && ( make clean ; $BINDIR/$1/bin/phpize ) && \
        ./configure --with-php-config=$BINDIR/$1/bin/php-config  && \
        make -j $JOBS && make install || unset WITH_TIMECOP \
    )

    ( [ -v WITHOUT_IMAGICK ] || [ -v ONLY_INI ] ) \
    && echo -e "$OMMIT Imagick deploy omitted \e[0m" \
    || ( \
        echo -e "$OK Compiling Imagick... \e[0m" ; \
        cd $BINDIR/imagick && $BINDIR/$1/bin/phpize && \
        ./configure --with-php-config=$BINDIR/$1/bin/php-config && \
        make -j $JOBS && make install || declare WITHOUT_IMAGICK= \
    )

    echo -e "$OMMIT Generating php.ini... \e[0m" ; cd $BINDIR && cat php.ini | tee $1/lib/php.ini && \
    ( [ -v WITHOUT_XDEBUG ] || cat tpl/xdebug.ini | tee -a $BINDIR/$1/lib/php.ini ) && \
    ( [ ! -v WITH_SYCK ] || cat tpl/syck.ini | tee -a $BINDIR/$1/lib/php.ini ) && \
    ( [ -v WITHOUT_APCU ] || cat tpl/apcu.ini | tee -a $BINDIR/$1/lib/php.ini ) && \
    ( [ ! -v WITH_GEARMAN ] || cat tpl/gearman.ini | tee -a $BINDIR/$1/lib/php.ini ) && \
    ( [ ! -v WITH_SWOOLE ] || cat tpl/swoole.ini | tee -a $BINDIR/$1/lib/php.ini ) && \
    ( [ ! -v WITH_REDIS ] || cat tpl/redis.ini | tee -a $BINDIR/$1/lib/php.ini ) && \
    ( [ ! -v WITH_TIMECOP ] || cat tpl/timecop.ini | tee -a $BINDIR/$1/lib/php.ini ) && \
    ( [ -v WITHOUT_IMAGICK ] || cat tpl/imagick.ini | tee -a $BINDIR/$1/lib/php.ini ) && \
    rm -rf $BINDIR/$1/etc && ln -nsf $BINDIR/etc $BINDIR/$1/
    $BINDIR/$1/bin/php --version && echo -e "$OK Build $1 complete sucessfully \e[0m" || \
    echo -e "$ERROR Failed to complete build $1 \e[0m"
fi

cd $RUNDIR
