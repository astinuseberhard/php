# **Bash script for compiling and installing any PHP version since 5**

Usage: `./build [-h?] [-f] [ [-i | -r] [-axs] [-u] <php_version_tag> ]`


## Features
- one script
- fetch PHP source from it's GitHub repository
- compile and install XDebug
- compile and install ACPu
- compile and install Syck (use only with PHP5)
- shows available versions (tags)
- updates system alternatives
- php.ini templates
- builds into subdirectory


## How to use
1. run ./build -f (fetch all sources)
2. customize configure.default file if necessery
3. customize php.ini and templates in tpl subdir if necessery
4. run ./build [options] [version] to prepare build
5. run ./build -i -u [version] to update system alternatives for existing build


## Notice
- You can use any tag, branch or even commit hash as a version (but only stable tags are fully supported)
- Script assumes that builds exists in subdirectories, which names match builds versions (php-7.1.10 is tag and subdir name for build 7.1.10)
- Script assumes that PHP source exists under php-src subdir, and any other sources (like extensions) has their own, known subdir
- Script clones missing sources from GitHub - you do not have to do it manually
- You may encounter configuration and/or compile errors - you have to alter your system accordingly, like install missing dependencies and libraries
- Script do not manage extensions versions - if you encounter any configuration and/or compile errors for any extension (like ACPu) - try to checkout into different extension version